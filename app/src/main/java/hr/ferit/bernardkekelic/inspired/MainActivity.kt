package hr.ferit.bernardkekelic.inspired

import android.content.res.Resources
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var myToastMessage: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        imageFirstPerson.setOnClickListener{onClickFirstImage()}
        imageSecondPerson.setOnClickListener{onClickSecondImage()}
        imageThirdPerson.setOnClickListener{onClickThirdImage()}

        setUpUI()
    }

    fun setUpUI(){

        var firstPerson = InspiringPerson(
            getString(R.string.FirstPersonName),
            getString(R.string.FirstPersonAge),
            getString(R.string.FirstPersonCV))

        var secondPerson = InspiringPerson(
            getString(R.string.SecondPersonName),
            getString(R.string.SecondPersonAge),
            getString(R.string.SecondPersonCV))

        var thirdPerson = InspiringPerson(
            getString(R.string.ThirdPersonName),
            getString(R.string.ThirdPersonAge),
            getString(R.string.ThirdPersonCV))

        TextNameFirstPerson.text = firstPerson.name
        textNameSecondPerson.text = secondPerson.name
        textNameThirdPerson.text = thirdPerson.name

        TextAgeFirstPerson.text = firstPerson.age
        textAgeSecondPerson.text = secondPerson.age
        textAgeThirdPerson.text = thirdPerson.age

        TextCVFirstPerson.text = firstPerson.cv
        textCVSecondPerson.text = secondPerson.cv
        textCVThirdPerson.text = thirdPerson.cv

        imageFirstPerson.setImageResource(R.drawable.alan_kay)
        imageSecondPerson.setImageResource(R.drawable.edsger_dijkstra)
        imageThirdPerson.setImageResource(R.drawable.alan_turing)
    }

    private fun getRandomNum(): Int {
        return (0..1).random()
    }

    fun onClickFirstImage(){
        if(getRandomNum() == 0) myToastMessage = getString(R.string.quote1FirstPerson)
        else myToastMessage = getString(R.string.quote2FirstPerson)

        showMyToast(myToastMessage)
    }
    fun onClickSecondImage(){
        if(getRandomNum() == 0) myToastMessage = getString(R.string.quote1SecondPerson)
        else myToastMessage = getString(R.string.quote2SecondPerson)

        showMyToast(myToastMessage)
    }
    fun onClickThirdImage(){
        if(getRandomNum() == 0) myToastMessage = getString(R.string.quote1ThirdPerson)
        else myToastMessage = getString(R.string.quote2ThirdPerson)

        showMyToast(myToastMessage)
    }
    private fun showMyToast(message: String){
        Log.d("BER", message)
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }


    class InspiringPerson constructor (val name: String, val age : String, val cv: String){

    }


}
